<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('index');
});

Route::get('/register', function (){
    return view('Form');
});
Route::get('/welcome',function () {
    return view('Welcome');
});

Route::get('/','HomeController@index');

Route::get('/form','AuthController@index');
Route::post('/kirim','AuthController@kirim');

Route::get('/data-tables',function(){
    return view('halaman.datatable');
});

Route::get('/table',function(){
    return view('halaman.table');
});

Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');